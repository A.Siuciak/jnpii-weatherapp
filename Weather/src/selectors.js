import { getStyle } from './style/style.js'
import { getWeatherParams } from './weather/weather.js'
import { createSelector } from 'reselect'

/* Query selector providing access to precise
 * description of actual query. Depends on 3 values:
 * state.searchBy, state.weatherMode (those 2 are
 * boolen, so we store them as a number from range [0,3]:
 * queryCode parameter) and state.location (actual state of
 * input field). This also allows to store last query by
 * passing an actual one as a parameter with "SEARCH" action. */
const querySel = (state) => {
    let json =  {
      'queryCode': (state.searchBy * 2) + state.weatherMode,
      'location': state.location
    };
    return JSON.stringify(json);
}
  
// Style selector using function from './style/style.js'.
const styleSel = (state) => {
    return getStyle(state);
}
  
// Weather parameters selector using function from './weather/weather.js'.
const weatherSel = (state) => {
    return getWeatherParams(state.weather);
}
 
// We export selectors created with createSelector.
const querySelector = createSelector(querySel, (val) => val)
const styleSelector = createSelector(styleSel, (val) => val)
const weatherSelector = createSelector(weatherSel, (val) => val)

export { querySelector, styleSelector, weatherSelector }
