import React from 'react'
import { connect } from 'react-redux'
import { getWeather } from '../API/weather.js'
import { getGIF } from '../API/GIF.js'
import  { querySelector, styleSelector } from '../selectors.js'

import Suggestions from './Suggestions.js'

// Component containg searching form.
class Form extends React.Component {
    constructor(props) {
        super(props)
        this.empty = this.empty.bind(this)
        this.handleInput = this.handleInput.bind(this)
        this.search = this.search.bind(this)
        this.setWeatherMode = this.setWeatherMode.bind(this)

        // This interval will be handling GIF display after searching.
        this.interval = setInterval(() => {
        //console.log('I am doing nothing...')
        }, 10000);
    }
    empty(e) {
        // I do nothing
    }
    
    handleInput(e){
        e.preventDefault();
        this.props.dispatch({ type: "INPUT", data: e.target.value});
    }
    
    // Searching for weather forecast.
    search(e){
        e.preventDefault();

        // Do not call API when actual query is same as last one.
        if (this.props.query !== this.props.lastQuery) {
            this.props.dispatch({ type: "SEARCH", data: this.props.query});
            getWeather(this.props.searchBy, this.props.location, this.props.weatherMode)
            .then((weather) => {
                // Getting keyword for new GIFs.
                let type = weather[0]["weather"][0]["description"];

                getGIF(type, 0).then((GIF) => {
                // If GIFs found, set props.GIF and app's interval.
                this.props.dispatch({type: "GIF", data: GIF});
                clearInterval(this.interval);
                this.interval = setInterval(() => {
                    getGIF(type, Math.floor(Math.random() * 5)).then((GIF) => {
                    this.props.dispatch({type: "GIF", data: GIF});
                    })
                }, 30000);
                }).finally(() => {
                    this.props.dispatch({type: "RECEIVED", data: JSON.stringify(weather)});
                })
            }).catch(() => {
            this.props.dispatch({ type: "ERROR"});
            })
        }
    }

    // Weather mode - hourly / daily.
    setWeatherMode(e){
        this.props.dispatch({ type: "WEATHER_MODE"});
    }

    render() {
        return (
            <form onSubmit={this.search} style={this.props.style.formDisplay} autoComplete="off">
                <input type="radio" id="hourly" name="weatherMode" value={0}
                checked={(this.props.weatherMode === 0)}
                onChange={this.setWeatherMode}>
                </input>
                <label htmlFor="hourly" style={this.props.style.text}>
                Hourly forecast for 2 days</label>
                <input type="radio" id="daily" name="weatherMode" value={1}
                checked={(this.props.weatherMode === 1)}
                onChange={this.setWeatherMode}>
                </input>
                <label htmlFor="daily" style={this.props.style.text}>
                Daily forecast for 7 days</label>
                <br></br>

                <input type="text" placeholder="Enter your location" id="txt"
                onInput={this.handleInput}
                onChange={this.empty} // added just to avoid warnings because of no onChange event
                style={this.props.style.input}
                value={this.props.location}>
                </input>
                <Suggestions />
                <br></br>
                <button type="submit">Search</button>
            </form>
        )
    }
}

// Props determining look and functioning of form.
const mapStateToProps = (state) => ({
    location: state.location,
    searchBy: state.searchBy,
    weatherMode: state.weatherMode,
    mode: state.mode,
    status: state.status,
    query: querySelector(state),
    style: styleSelector(state),
    lastQuery: state.lastQuery
})

export default connect(mapStateToProps)(Form)