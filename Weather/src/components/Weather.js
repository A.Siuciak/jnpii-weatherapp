import React from 'react'
import { connect } from 'react-redux'
import  { styleSelector, weatherSelector } from '../selectors.js'
import { Image } from '../style/styledComponents.js'

class Weather extends React.Component {
    render() {
        return (
            <div id="weather" style={this.props.style.weatherDisplay}>
                <Image id="GIF1" src={this.props.GIF} alt="GIF1"></Image>
                <div id="weather-data">
                    <p id="type" style={this.props.style.text}>
                    {this.props.weatherData.type + " weather"}</p>
                    <p className="param" style={this.props.style.text}>
                    {"Average temperature: " + this.props.weatherData.avg + "\xB0C"}</p>
                    <p className="param" style={this.props.style.text}>
                    {"Max temperature: " + this.props.weatherData.max + "\xb0C"}</p>
                    <p className="param" style={this.props.style.text}>
                    {"Min temperature: " + this.props.weatherData.min + "\xB0C"}</p>
                    <p className="param" style={this.props.style.text}>
                    {"Rainy " + this.props.weatherData.text + ": " +
                    this.props.weatherData.rainy}</p>
                </div>
                <Image id="GIF2" src={this.props.GIF} alt="GIF2"></Image>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    style: styleSelector(state),
    weatherData: weatherSelector(state),
    GIF: state.GIF
})

export default connect(mapStateToProps)(Weather)