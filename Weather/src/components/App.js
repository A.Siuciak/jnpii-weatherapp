import React from 'react';
import sun from '../pictures/sun.svg';
import { connect } from 'react-redux'
import  { styleSelector } from '../selectors.js'
import { Headline, Image } from '../style/styledComponents.js'

import Weather from './Weather.js'
import Form from './Form.js'
import Mode from './Mode.js'
import SearchingMode from './SearchingMode.js'
import Loading from './Loader.js'
import '../style/app.css';

// Main component.
class App extends React.Component{
  constructor(props) {
    super(props)
    this.click = this.click.bind(this)
  }

  
  // Managing key pressing.
  click(e) {
    // Enter/Tab key - fills input with value from highlighted suggestion.
    if (e.keyCode === 13 || e.keyCode === 9) {

      // Change handling only if suggestions are displayed.
      if (this.props.showSuggestions &&
          this.props.location &&
          this.props.displayedSuggestions.length > 0 &&
          this.props.searchBy === 0) {
        e.preventDefault();
        const chosen = this.props.displayedSuggestions[this.props.activeSuggestion]
        this.props.dispatch({ type: "INPUT", data: chosen});
        this.props.dispatch({ type: "HIDE_SUGGESTIONS" })
      }
    }

    // Up arrow - switch suggestions.
    if (e.keyCode === 38) {
      // Change handling only if suggestions are displayed.
      if (this.props.showSuggestions &&
          this.props.location &&
          this.props.displayedSuggestions.length > 0 &&
          this.props.searchBy === 0) {
            e.preventDefault();
            if (this.props.activeSuggestion > 0) {
              const newActive = this.props.activeSuggestion - 1
              this.props.dispatch({ type: "UPDATE_ACTIVE", data: newActive})
            }
      }
    }

    // Down arrow - switch suggestions.
    if (e.keyCode === 40) {
      // Change handling only if suggestions are displayed.
      if (this.props.showSuggestions &&
          this.props.location &&
          this.props.displayedSuggestions.length > 0 &&
          this.props.searchBy === 0) {
            e.preventDefault();
            if (this.props.activeSuggestion < this.props.displayedSuggestions.length - 1) {
              const newActive = this.props.activeSuggestion + 1
              this.props.dispatch({ type: "UPDATE_ACTIVE", data: newActive})
            }
      }
    }
  }

  render(){
    return (
      <div id="body" style={this.props.style.bodyBackground}
      onKeyDown={this.click}>
          <Headline style={this.props.style.text}>JNP II Weather</Headline>
          <SearchingMode />
          <br></br>
          <Form />
          <Mode />
          <Loading />
          <p id="error">{this.props.style.error}</p>
          <Image id="logo" src={sun} alt="pic"></Image>
          <Weather />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  location: state.location,
  showSuggestions: state.showSuggestions,
  searchBy: state.searchBy,
  displayedSuggestions: state.displayedSuggestions,
  activeSuggestion: state.activeSuggestion,
  style: styleSelector(state)
})

export default connect(mapStateToProps)(App);
