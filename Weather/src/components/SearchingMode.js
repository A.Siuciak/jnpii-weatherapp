import React from 'react'
import { connect } from 'react-redux'
import  { styleSelector } from '../selectors.js'

// Searching mode button (over the form)
class SearchingMode extends React.Component {
    constructor(props) {
        super(props)
        this.setSearching = this.setSearching.bind(this)
    }

    // Searching mode - city name / geolocation.
    setSearching(e){
        this.props.dispatch({ type: "SEARCHING_MODE" });
    }

    render() {
         return (
             <button onClick={this.setSearching}
             style={this.props.style.formDisplay}
             id="searchingMode">{this.props.style.butVal1}</button>
         )
    }
}

const mapStateToProps = (state) => ({
    style: styleSelector(state),
})

export default connect(mapStateToProps)(SearchingMode)