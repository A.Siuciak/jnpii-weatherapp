import React from 'react'
import { connect } from 'react-redux'
import  { styleSelector } from '../selectors.js'

// Mode button (top right corner)
class Mode extends React.Component {
    constructor(props) {
        super(props)
        this.setMode = this.setMode.bind(this)
    }
    setMode(e) {
        this.props.dispatch({ type: "MODE"})
    }
    render() {
         return (
             <button onClick={this.setMode} id="mode">{this.props.style.butVal2}</button>
         )
    }
}

const mapStateToProps = (state) => ({
    style: styleSelector(state),
})

export default connect(mapStateToProps)(Mode)