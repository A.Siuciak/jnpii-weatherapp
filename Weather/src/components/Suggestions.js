import React from 'react'
import { connect } from 'react-redux'
import  { styleSelector } from '../selectors.js'
import { List } from '../style/styledComponents.js'

// Suggestions list - subcomponent of form.
class Suggestions extends React.Component{
    constructor(props) {
        super(props)
        this.enterSuggestion = this.enterSuggestion.bind(this)
    }

    // Handling of suggestion click.
    enterSuggestion(e) {
        /* After clicking on suggestion, input gets its content
        (and so do this.props.location). */
        this.props.dispatch({ type: "INPUT", data: e.currentTarget.innerText})
        this.props.dispatch({ type: "HIDE_SUGGESTIONS"})
    }
  
    render() {
        let suggestionsList;
        if (this.props.location && this.props.showSuggestions) {
            suggestionsList = (
                <List id="suggestions">
                {this.props.displayedSuggestions.map((suggestion, index) => {
                    let id;
                    // Give the active suggestion an id.
                    if (index === this.props.activeSuggestion) {
                        id = "active";
                    }
                    return (
                        <li id={id} key={index}
                        onClick={this.enterSuggestion}>{suggestion}
                        </li>
                    );
                })}
                </List>
            )
        }
        return (
            <div id="sugg" style={this.props.style.input}>
                {suggestionsList}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.location,
    showSuggestions: state.showSuggestions,
    style: styleSelector(state),
    displayedSuggestions: state.displayedSuggestions,
    activeSuggestion: state.activeSuggestion
})

export default connect(mapStateToProps)(Suggestions)