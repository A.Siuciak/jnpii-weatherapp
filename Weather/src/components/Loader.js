import Loader from 'react-loader-spinner'
import React from 'react'
import { connect } from 'react-redux';
 
// Loading component, displayed only while waiting for searching results.
class Loading extends React.Component {
    render() {
     return(
      <Loader
         visible={this.props.status === 'fetching'}
         type="ThreeDots"
         color="green"
         height={150}
         width={150}
      />
     );
    }
}

const mapStateToProps = (state) => ({
    status: state.status,
})

export default connect(mapStateToProps)(Loading)
