import styled from 'styled-components'

// Some styled components.

const Headline = styled.h1`
font-size: 40px;
font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
font-weight: 600;
`

const List = styled.ul`
position: relative;
list-style: none;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
right: 25px;
`

const Image = styled.img`
width: 200px;
height: 200px;
`

export { Headline, List, Image }