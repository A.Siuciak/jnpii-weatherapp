/* Interface of functions creating style props depending
 on app's state. */


// Body background depends on mode (light/dark)
const getBodyBackground = (mode) => {
    if (mode === 0) {
        return {
            'backgroundColor': 'wheat'
        }
    } else {
        return {
            'backgroundColor': '#00001a'
        }
    }
}

// Text color as well.
const getTextColor = (mode) => {
    if (mode === 0) {
        return {
            'color': '#660000'
        }
    } else {
        return {
            'color': 'aliceblue'
        }
    }
}

// Input is displayed only if we want to search by city name.
const getInputDisplay = (searchBy) => {
    if (searchBy === 0) {
        return {
            'display': 'inline'
        }
    } else {
        return {
            'display': 'none'
        }
    }
}

// Error is not empty only after failure in searching.
const getError = (status) => {
    if (status === 'error') {
        return "Sorry, we didn't find weather forecast for your location."
    } else {
        return ""
    }
}

// Loader is displayed only during fetching.
const getLoaderDisplay = (status) => {
    if (status === 'fetching') {
        return {
            display: 'inline'
        }
    } else {
        return {
            display: 'none'
        }
    }
}

// Weather is displayed only if searching succeed.
const getWeatherDisplay = (weather, status) => {
    if (weather && status === 'success') {
        return {
            display: 'grid'
        }
    } else {
        return {
            display: 'none'
        }
    }
}

// Do not display form while waiting for results.
const getFormDisplay = (status) => {
    if (status === 'fetching') {
        return {
            display: 'none'
        }
    } else {
        return {
            display: 'inline'
        }
    }
}

// Changing mode button contains name of opposite mode.
const modeButtonValue = (mode) => {
    if (mode === 0) {
        return 'Dark mode'
    } else {
        return 'Light mode'
    }
}

// Changing search mode button contains name of opposite mode.
const searchButtonValue = (searchBy) => {
    if (searchBy === 0) {
        return 'Search by geolocation'
    } else {
        return 'Search by city name'
    }
}
 
/* Function used by styleSelector. Using all functions above, calculates object
 * containing info about how the page should look like, depending on app's state. */
const getStyle = (state) => {
    return {
        'bodyBackground': getBodyBackground(state.mode),
        'text': getTextColor(state.mode),
        'input': getInputDisplay(state.searchBy),
        'error': getError(state.status),
        'loaderDisplay': getLoaderDisplay(state.status),
        'weatherDisplay' : getWeatherDisplay(state.weather, state.status),
        'butVal2': modeButtonValue(state.mode),
        'butVal1': searchButtonValue(state.searchBy),
        'formDisplay': getFormDisplay(state.status)
    }
}

export { getStyle }