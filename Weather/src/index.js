import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable'
import { Provider } from 'react-redux';
import { reducer } from './reducer.js'
import { showSuggestionsEpic, updateSuggestionsEpic,
  hideSuggestionsEpic, clearInputEpic} from './epics.js'

// Supplying the initial state.
const initial = {
  // Actual state of input field
  location: '',
  // Searching option - 0 = by city, 1 = by geolocation.
  searchBy: 0,
  // Result display option - 0 = hourly for 2 days, 1 = daily for 7 days.
  weatherMode: 0,
  // Mode - 0 = 'light', 1 = 'dark'
  mode: 0,
   /* 'initial' at the beginning,
      'fetching' when waiting for searching results,
      'success' when results have been found,
      'error' when have not */
  status: 'initial',
  // Actually displayed weather.
  weather: '',
  // Link to actually displayed GIF, initial one is just random.
  GIF: 'https://media.tenor.com/images/78edbb1f8c34b17b20e9e0987914001e/tenor.gif',
  // Last query requested, encoded in specific format.
  lastQuery: '',
  // Display of suggestions:
  showSuggestions: false,
  // Actually displayed suggestions:
  displayedSuggestions: ['War', 'p'],
  // Actually highlighted suggestion:
  activeSuggestion: 0
}

const epicMiddleware = createEpicMiddleware()

const store = createStore(reducer,
  applyMiddleware(epicMiddleware)
);

epicMiddleware.run(showSuggestionsEpic)
epicMiddleware.run(updateSuggestionsEpic)
epicMiddleware.run(hideSuggestionsEpic)
epicMiddleware.run(clearInputEpic)

const Application = () => {
  return <Provider store={store}>
    <App />
  </Provider>
}

ReactDOM.render(<Application />, document.getElementById('root'));

export { initial }