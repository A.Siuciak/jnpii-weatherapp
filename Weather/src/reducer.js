import { initial } from './index.js'
import { updateSuggestions } from './cities/city.js'

// A reducer for action handling.
const reducer = (state = initial, action) => {
    switch(action.type) {
      case "INPUT":
        return {
          location: action.data,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: state.status,
          weather: state.weather,
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        };
      case "SEARCH":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: 'fetching',
          weather: state.weather,
          GIF: state.GIF,
          lastQuery: action.data, // saving code of recently requested query
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "ERROR":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: 'error',
          weather: '', // do not display any weather after bad request
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "RECEIVED":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: 'success',
          weather: action.data,
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "MODE":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: 1 - state.mode,
          status: state.status,
          weather: state.weather,
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "SEARCHING_MODE":
        return {
          location: state.location,
          searchBy: 1 - state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: state.status,
          weather: state.weather,
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "WEATHER_MODE":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: 1 - state.weatherMode,
          mode: state.mode,
          status: state.status,
          weather: state.weather,
          GIF: state.GIF,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
      case "GIF":
        return {
          location: state.location,
          searchBy: state.searchBy,
          weatherMode: state.weatherMode,
          mode: state.mode,
          status: state.status,
          weather: state.weather,
          GIF: action.data,
          lastQuery: state.lastQuery,
          showSuggestions: state.showSuggestions,
          displayedSuggestions: state.displayedSuggestions,
          activeSuggestion: state.activeSuggestion
        }
        case "SHOW_SUGGESTIONS":
          return {
            location: state.location,
            searchBy: state.searchBy,
            weatherMode: state.weatherMode,
            mode: state.mode,
            status: state.status,
            weather: state.weather,
            GIF: state.GIF,
            lastQuery: state.lastQuery,
            showSuggestions: true,
            displayedSuggestions: state.displayedSuggestions,
            activeSuggestion: 0
          }
        case "HIDE_SUGGESTIONS":
          return {
            location: state.location,
            searchBy: state.searchBy,
            weatherMode: state.weatherMode,
            mode: state.mode,
            status: state.status,
            weather: state.weather,
            GIF: state.GIF,
            lastQuery: state.lastQuery,
            showSuggestions: false,
            displayedSuggestions: state.displayedSuggestions,
            activeSuggestion: state.activeSuggestion
          }
        case "UPDATE_SUGGESTIONS":
          return {
            location: state.location,
            searchBy: state.searchBy,
            weatherMode: state.weatherMode,
            mode: state.mode,
            status: state.status,
            weather: state.weather,
            GIF: state.GIF,
            lastQuery: state.lastQuery,
            showSuggestions: true,
            displayedSuggestions: updateSuggestions(state.location),
            activeSuggestion: state.activeSuggestion
          }
        case "UPDATE_ACTIVE":
          return {
            location: state.location,
            searchBy: state.searchBy,
            weatherMode: state.weatherMode,
            mode: state.mode,
            status: state.status,
            weather: state.weather,
            GIF: state.GIF,
            lastQuery: state.lastQuery,
            showSuggestions: state.showSuggestions,
            displayedSuggestions: state.displayedSuggestions,
            activeSuggestion: action.data
          }
      default:
        return state;
    }
  }

export { reducer }