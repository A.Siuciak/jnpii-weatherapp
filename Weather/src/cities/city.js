const list  = require('./city.list.json')

let cities = []
for (var i = 0; i < list.length; i++) {
    cities.push(list[i]["name"]);
}

// Maximum number of suggestions displayed.
const maxSuggestions = 7

const updateSuggestions = (input) => {
    const newSuggestions = cities.filter(
        suggestion =>
          suggestion.toLowerCase().indexOf(input.toLowerCase()) > -1
    );
    const newTopSuggestions = newSuggestions.slice(0, maxSuggestions);
    return newTopSuggestions
}

export { updateSuggestions }