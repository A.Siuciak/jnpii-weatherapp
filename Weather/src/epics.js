// Interface of epics.

import { filter, mapTo } from 'rxjs/operators';

// Showing suggestions after input change.
const showSuggestionsEpic = action$ => action$.pipe(
    filter(action => action.type === 'INPUT'),
    mapTo({ type: 'SHOW_SUGGESTIONS' })
);

// Update suggestions displayed after input change.
const updateSuggestionsEpic = action$ => action$.pipe(
    filter(action => action.type === 'INPUT'),
    mapTo({ type: 'UPDATE_SUGGESTIONS'})
);

// Hide suggestions after running a query.
const hideSuggestionsEpic = action$ => action$.pipe(
    filter(action => action.type === 'SEARCH'),
    mapTo({ type: 'HIDE_SUGGESTIONS' })
);

// Clear input after searching mode change.
const clearInputEpic = action$ => action$.pipe(
    filter(action => action.type === 'SEARCHING_MODE'),
    mapTo({ type: 'INPUT', data: '' })
);

export { showSuggestionsEpic, updateSuggestionsEpic,
        hideSuggestionsEpic, clearInputEpic}